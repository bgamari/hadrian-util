{
  description = "hadrian-util";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        mkHU = pkgs:
          (pkgs.haskellPackages.callCabal2nix "hadrian-util" ./. {}).overrideAttrs (oldAttrs: {
            postInstall = ''
              mkdir $out/libexec
              $out/bin/hadrian-util --bash-completion-script $out/bin/hadrian-util > $out/libexec/bash-completion
            '';
          });
      in
      {
        packages = rec {
          hadrian-util = mkHU pkgs;
          default = hadrian-util;
        };

        apps = rec {
          hadrian-util = flake-utils.lib.mkApp { drv = self.packages.${system}.hadrian-util; };
          default = hadrian-util;
        };

        overlays = rec {
          default = final: prev: {
            hadrian-util = mkHU prev;
          };
        };
      }
    );
}
