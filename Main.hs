import qualified Data.List as L
import Data.Maybe
import Control.Monad
import System.Environment (lookupEnv)
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.Text as T
import qualified Data.Text.IO as T

import Options.Applicative
import System.FilePath
import System.Process.Typed
import System.Directory
import System.IO

-- | A (canonicalized) path to a GHC working tree.
newtype SourceRoot = SourceRoot { sourceRootPath :: FilePath }
    deriving (Show, Eq, Ord)

-- | A Hadrian build root name (e.g. @_build@).
newtype BuildRoot = BuildRoot { buildRootName :: String }
    deriving (Show, Eq, Ord)

newtype FlavourName = FlavourName { flavourName :: String }
    deriving (Show, Eq, Ord)

buildRootDir :: BuildRoot -> FilePath
buildRootDir (BuildRoot name) =
    case name of
      "" -> "_build"
      _  -> "_build-" ++ name

buildRootPath :: SourceRoot -> BuildRoot -> FilePath
buildRootPath root buildRoot =
    sourceRootPath root </> buildRootDir buildRoot

buildRootFlavourPath :: SourceRoot -> BuildRoot -> FilePath
buildRootFlavourPath root buildRoot =
    buildRootPath root buildRoot </> ".flavour"

-- | Find the 'SourceRoot' of the current working directory.
findSourceRoot :: IO SourceRoot
findSourceRoot = do
    cwd <- getCurrentDirectory
    let prefixes :: [FilePath]
        prefixes = map joinPath $ reverse $ L.inits $ splitPath cwd
    go prefixes
  where
    go [] = fail "failed to find hadrian/ directory"
    go (path:rest) = do
        exists <- doesFileExist (path </> "hadrian" </> "hadrian.cabal")
        if exists
           then SourceRoot <$> canonicalizePath path
           else go rest

-- | The path to the hadrian executable (or rather, wrapper script).
hadrianExecPath :: SourceRoot -> FilePath
hadrianExecPath (SourceRoot root) =
    root </> "hadrian" </> "build-cabal"

processConfig :: SourceRoot -> [String] -> ProcessConfig () () ()
processConfig root args = do
    setWorkingDir (sourceRootPath root) (proc (hadrianExecPath root) args)

readFileIfExists :: FilePath -> IO (Maybe String)
readFileIfExists fpath =
    withFileIfExists fpath (fmap T.unpack . T.hGetContents)

withFileIfExists :: FilePath -> (Handle -> IO a) -> IO (Maybe a)
withFileIfExists fpath action = do
    exists <- doesFileExist fpath
    if exists
      then Just <$> withFile fpath ReadMode action
      else return Nothing

getBuildRootFlavour :: SourceRoot -> BuildRoot -> IO FlavourName
getBuildRootFlavour root buildRoot = do
    mb_name <- withFileIfExists flavourFile T.hGetContents
    return $ FlavourName $ maybe "default" (T.unpack . T.strip) mb_name
  where
    flavourFile = buildRootFlavourPath root buildRoot

setBuildRootFlavour :: SourceRoot -> BuildRoot -> FlavourName -> IO ()
setBuildRootFlavour root buildRoot (FlavourName flavour) = do
    writeFile flavourFile flavour
  where
    flavourFile = buildRootFlavourPath root buildRoot

buildRootDefaultArgsPath :: SourceRoot -> BuildRoot -> FilePath
buildRootDefaultArgsPath root buildRoot =
    buildRootPath root buildRoot </> ".default-args"

getBuildRootDefaultArgs :: SourceRoot -> BuildRoot -> IO [String]
getBuildRootDefaultArgs root buildRoot = do
    maybe [] lines <$> readFileIfExists (buildRootDefaultArgsPath root buildRoot)

setBuildRootDefaultArgs :: SourceRoot -> BuildRoot -> [String] -> IO ()
setBuildRootDefaultArgs root buildRoot args = do
    writeFile (buildRootDefaultArgsPath root buildRoot) (unlines args)

-- | The path to the @hadrian-util@ configuration directory.
configDir :: SourceRoot -> FilePath
configDir root = sourceRootPath root </> ".hadrian-util"

-- | The path to the default build root state file.
defaultBuildRootFile :: SourceRoot -> FilePath
defaultBuildRootFile root = configDir root </> "default-build-root"

-- | Get the default build root state.
getDefaultBuildRoot :: SourceRoot -> IO BuildRoot
getDefaultBuildRoot root = do
    BuildRoot . fromMaybe "" <$> readFileIfExists (defaultBuildRootFile root)

-- | Get the default build root state if one was not given explicitly.
getDefaultBuildRootOr :: SourceRoot -> Maybe BuildRoot -> IO BuildRoot
getDefaultBuildRootOr root =
    maybe (getDefaultBuildRoot root) pure

-- | Set the default build root state.
setDefaultBuildRoot :: SourceRoot -> BuildRoot -> IO ()
setDefaultBuildRoot root buildRoot = do
    createDirectoryIfMissing False (configDir root)
    writeFile (defaultBuildRootFile root) (buildRootName buildRoot)

runHadrian :: Maybe BuildRoot -> [String] -> IO ()
runHadrian mbBuildRoot args = do
    root <- findSourceRoot
    buildRoot <- getDefaultBuildRootOr root mbBuildRoot
    flavour <- getBuildRootFlavour root buildRoot
    defArgs <- getBuildRootDefaultArgs root buildRoot
    let args' = [ "--build-root="++buildRootDir buildRoot
                , "--flavour="++flavourName flavour
                ] ++ defArgs ++ args
    putStrLn $ unwords args'
    runProcess_ (processConfig root args')

hadrianSettings :: SourceRoot -> BuildRoot -> FilePath
hadrianSettings root buildRoot =
    buildRootPath root buildRoot </> "hadrian.settings"

catConfig :: Maybe BuildRoot -> IO ()
catConfig mbBuildRoot = do
    root <- findSourceRoot
    buildRoot <- getDefaultBuildRootOr root mbBuildRoot
    putStrLn . fromMaybe "" =<< readFileIfExists (hadrianSettings root buildRoot)

editConfig :: Maybe BuildRoot -> IO ()
editConfig mbBuildRoot = do
    root <- findSourceRoot
    buildRoot <- getDefaultBuildRootOr root mbBuildRoot
    editor <- fromMaybe "vi" <$> lookupEnv "EDITOR"
    runProcess_ (proc editor [hadrianSettings root buildRoot])

setConfig :: String -> Maybe BuildRoot -> String -> String -> IO ()
setConfig op buildRoot key val = do
    root <- findSourceRoot
    buildRoot <- getDefaultBuildRootOr root buildRoot
    appendFile (hadrianSettings root buildRoot) $ unwords [key, op, val, "\n"]

findBuildRoots :: SourceRoot -> IO [BuildRoot]
findBuildRoots root = do
    ents <- listDirectory (sourceRootPath root)
    return $ mapMaybe isBuildRoot ents
  where
    isBuildRoot :: FilePath -> Maybe BuildRoot
    isBuildRoot "_build" = Just (BuildRoot "")
    isBuildRoot path
      | Just name <- "_build-" `L.stripPrefix` path = Just (BuildRoot name)
    isBuildRoot _ = Nothing

listBuildRoots :: IO ()
listBuildRoots = do
    root <- findSourceRoot
    buildRoots <- findBuildRoots root
    flavours <- mapM (getBuildRootFlavour root) buildRoots
    defaultRoot <- getDefaultBuildRoot root

    let format buildRoot flavour =
            prefix ++ buildRootName buildRoot ++ "     (" ++ buildRootPath root buildRoot ++ ")    [flavour=" ++ flavourName flavour ++ "]"
          where
            prefix
              | buildRoot == defaultRoot = "* "
              | otherwise                = "  "
    putStrLn $ unlines
      $  ["Build roots in " <> sourceRootPath root <> ":"]
      ++ zipWith format buildRoots flavours

initBuildRoot :: BuildRoot -> FlavourName -> Bool -> IO ()
initBuildRoot buildRoot flavour makeDefault = do
    root <- findSourceRoot
    createDirectory (buildRootPath root buildRoot)
    setBuildRootFlavour root buildRoot flavour
    when makeDefault (setDefaultBuildRoot root buildRoot)

rmBuildRoot :: Maybe BuildRoot -> Bool -> IO ()
rmBuildRoot _mbBuildRoot False = fail "give --force flag to confirm"
rmBuildRoot mbBuildRoot True = do
    root <- findSourceRoot
    buildRoot <- getDefaultBuildRootOr root mbBuildRoot
    removeDirectoryRecursive (buildRootPath root buildRoot)
    putStrLn $ "Removed build root `" <> buildRootName buildRoot <> "`"

completeBuildRoot :: Completer
completeBuildRoot = mkCompleter $ \s -> do
    root <- findSourceRoot
    buildRoots <- findBuildRoots root
    return $ filter (s `L.isPrefixOf`) $ map buildRootName buildRoots

completeSetting :: Completer
completeSetting = mkCompleter $ \s -> do
    root <- findSourceRoot
    out <- readProcessStdout_ (processConfig root ["autocomplete", "--no-time", "--quiet", "--complete-setting="++s])
    return $ map BS.unpack (BS.lines out)

arguments :: Parser (IO ())
arguments =
    hsubparser (mconcat
    [ command "config" (info configMode (progDesc "Things to do with hadrian.settings"))
    , command "build-root" (info buildRootMode (progDesc "Things to do with build roots"))
    , command "run" (info runHadrianMode (progDesc "Run hadrian" <> forwardOptions))
    , command "set-default-args" (info setDefaultArgsMode (progDesc "Set default arguments of the current or given build root"))
    ])
    <|> runHadrianMode
  where
    runHadrianMode :: Parser (IO ())
    runHadrianMode =
        runHadrian <$> optional buildRootArg <*> hadrianArgs

    setDefaultArgsMode :: Parser (IO ())
    setDefaultArgsMode =
        setDefaultArgs
        <$> optional buildRootArg
        <*> many (argument str (help "Arguments to pass to Hadrian invocations"))
      where
        setDefaultArgs mbBuildRoot args = do
          root <- findSourceRoot
          buildRoot <- getDefaultBuildRootOr root mbBuildRoot
          setBuildRootDefaultArgs root buildRoot args

    hadrianArgs :: Parser [String]
    hadrianArgs = many $ argument str (help "hadrian flags")

    configMode :: Parser (IO ())
    configMode = hsubparser $ mconcat
        [ command "cat" (info catMode (progDesc "Print hadrian.settings to stdout"))
        , command "edit" (info editMode (progDesc "Open hadrian.settings in an editor"))
        , command "set" (info setMode (progDesc "Set (with `=`) a hadrian.settings key"))
        , command "append" (info appendMode (progDesc "Append (with `+=`) a hadrian.settings key"))
        ]
      where
        catMode = catConfig <$> optional buildRootArg
        editMode = editConfig <$> optional buildRootArg
        setMode = setConfig "=" <$> optional buildRootArg  <*> settingArg <*> valueArg
        appendMode = setConfig "+=" <$> optional buildRootArg  <*> settingArg <*> valueArg
        settingArg = argument str (completer completeSetting <> metavar "SETTING" <> help "hadrian.settings key")
        valueArg = fmap unwords $ some $ argument str (help "Value")

    buildRootMode :: Parser (IO ())
    buildRootMode = hsubparser $ mconcat
        [ command "list" (info listMode (progDesc "List build roots"))
        , command "init" (info initMode (progDesc "Create new build root"))
        , command "rm" (info rmMode (progDesc "Delete a build root"))
        , command "set-default" (info setDefaultMode (progDesc "Set default build root"))
        , command "set-flavour" (info setFlavourMode (progDesc "Set flavour of a build root"))
        ]
      where
        listMode = pure listBuildRoots
        initMode =
            initBuildRoot
            <$> argument (BuildRoot <$> str)
                         (metavar "NAME" <> help "new build root name")
            <*> flavourArg
            <*> switch (long "default" <> short 'd' <> help "make default build root")
        rmMode =
            rmBuildRoot
            <$> optional buildRootArg
            <*> switch (long "force" <> short 'f' <> help "force")
        setDefaultMode =
            run <$> buildRootArg
          where
            run buildRoot = do
                root <- findSourceRoot
                setDefaultBuildRoot root buildRoot
        setFlavourMode =
            run <$> buildRootArg <*> flavourArg
          where
            run buildRoot flavour = do
                root <- findSourceRoot
                setBuildRootFlavour root buildRoot flavour

    flavourArg :: Parser FlavourName
    flavourArg =
        option (FlavourName <$> str) (long "flavour" <> short 'f' <> help "base flavour name")

    buildRootArg :: Parser BuildRoot
    buildRootArg =
        option (BuildRoot <$> str)
               (short 'd' <> long "build-root"
                <> completer completeBuildRoot
                <> help "which build root to use")

main :: IO ()
main =
    join $ execParser $ info (helper <*> arguments) mempty
